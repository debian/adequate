ADEQUATE 1
===========

# NAME

adequate - Debian package quality testing tool

# SYNOPSIS

**adequate** [*options*] *package-name*...

**adequate** [*options*] --all

**adequate** [*options*] --apt-preinst

**adequate** [*options*] --pending

**adequate** --list-tags

**adequate** --help

# DESCRIPTION

**adequate** checks packages installed on the system and reports bugs and policy
violations.

# OPTIONS

**--all**

> Run checks against all the installed packages.

**--tags** *tag1*[,*tag2*...]

> Emit only these tags.

**--tags** -*tag1*[,*tag2*...]

> Don't emit these tags.

**--debconf**

> Report issues via **debconf**(7).

**--fail**

> Exit with status 2 if at least one tag was emitted

**--root** *directory*

> Change the root directory (using **chroot**(2)).

**--user** *user*[:*group*]

> Switch user and group before running any checks.  This is most useful together
> with **--root** or **--pending** which require superuser privileges.

**--apt-preinst**

> Read APT configuration and *.deb* filenames from stdin, and append packages
> names to */var/lib/adequate/pending* for later processing (see **--pending**).

> This option is used internally by the APT hook. The hook is disabled by
> default; please edit *</etc/apt/apt.conf.d/20adequate* to enable it.

**--pending**

> Run checks against packages listed in */var/lib/adequate/pending*, then empty
> the file.

**--list-tags**

> List all tag names and exit.

**--help**

> Display help and exit.

# TAGS

**bin-or-sbin-binary-requires-usr-lib-library**

> This package ships a binary in /bin or /sbin that requires a library in /usr/lib.  Obsolete as of https://wiki.debian.org/UsrMerge

**broken-binfmt-detector**

> The detector registered with update-binfmts(8) does not exist.

**broken-binfmt-interpreter**

> The interpreter registered with update-binfmts(8) does not exist.

**broken-symlink**

> This package ships a symlink which points to a non-existent file.

**incompatible-licenses**

> Licenses of the libraries the binary is linked to are incompatible. Obsolete as of adequate version 0.16.19

**invalid-dbus-user-or-group**

> A D-Bus configuration file specifies an invalid user or group.

**invalid-systemd-user-or-group**

> A systemd service unit script specifies an invalid user or group.

**invalid-sysvinit-user-or-group**

> A sysvinit script specifies an invalid user or group.

**ldd-failure**

> Running "`ldd -r`" on the file failed unexpectedly.  This is most likely a bug in libc or adequate itself.

> https://bugs.debian.org/710521

**library-not-found**

> The binary is linked with a library, which cannot be found.

> Debian Policy §8.6

**missing-alternative**

> This package is a provider of the virtual package `x-terminal-emulator`, but it doesn't register itself as an alternative for `/usr/bin/x-terminal-emulator`; or it is a provider of the virtual package `x-window-manager`, but it doesn't register itself as an alternative for `/usr/bin/x-window-manager`.

> Debian Policy §11.8.3, Debian Policy §11.8.4

**missing-copyright-file**

> The copyright file for this package is missing.  This often happens if /usr/share/doc/<pkg>/ was a real directory in a previous version of the package, but it's now a symlink; dpkg never replaces directory with a symlink to a directory.

> Debian Policy §12.5, §6.6

**missing-pkgconfig-dependency**

> Dependency of a pkg-config (.pc) file shipped by this package couldn't be satisfied.

> Debian Policy §8.4

**missing-symbol-version-information**

> The binary uses versioned symbols, but the library provides only unversioned ones.

**obsolete-conffile**

> The current version of this package no longer ships a conffile (that used to be included in the past).  However, the conffile hasn't been removed on upgrade.

> https://wiki.debian.org/DpkgConffileHandling, dpkg-maintscript-helper(1)

**program-name-collision**

> This package ships a program with the same name as another program.

> Debian Policy §10.1

**py-file-not-bytecompiled**

> This package ships Python modules that are not byte-compiled.

> Python Policy §2.6

**pyshared-file-not-bytecompiled**

> This package ships Python modules in /usr/share/pyshared that are not byte-compiled.

> Python Policy §2.6, §1.5

**symbol-size-mismatch**

> The symbol has changed size since the package was built.  It might be an indication that the library broke ABI.  If ABI wasn't broken, and the library bumped shlibs (or symbols), the package should be binNMUed.

**undefined-symbol**

> The symbol has not been found in the libraries linked with the binary.  Either the binary either needs to be linked with an additional shared library, or the dependency on the shared library package that provides this symbol is too weak.

> Debian Policy §3.5, §8.6, §10.2


# EXAMPLES

**adequate coreutils**

>  Check the **coreutils** package.

**adequate --tags obsolete-conffile --all**

> Check all the packages for obsolete conffiles.

**adequate --tags -py-file-not-bytecompiled,pyshared-file-not-bytecompiled --all**

> Check all the packages, ignoring Python bytecompilation issues.

# EXIT STATUS

> 0    if no tags were emitted or --fail was not set

> 1    on invalid invocation

> 2    if at least one tag was emitted and --fail was set

# REPORTING BUGS

If you report a bug that was found by adequate, please use the following usertags:

> User: debian-qa@lists.debian.org

> Usertags: adequate *tag-name*

Please keep in mind that adequate is not perfect; therefore false positives are
possible. Don't report the bug unless you understand the underlying problem.
When in doubt, please ask at *debian-qa@lists.debian.org* first.

# SEE ALSO

Debian Policy:
*/usr/share/doc/debian-policy/policy.txt.gz*
or *https://www.debian.org/doc/debian-policy/*

Python Policy:
*/usr/share/doc/python/python-policy.txt.gz*
or *https://www.debian.org/doc/packaging-manuals/python-policy/*

**lintian**(1), **piuparts**(1)
