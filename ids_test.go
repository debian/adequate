package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

const sampleSyvinitFileContents = `
# ignore
# USER=ignore
FOOUSER = "foo-user"#blahblah
FOO_GROUP	= 	'foo-group' # blah
FOO_GROUP=

`

func TestExtractSysvinitIDsFromFile(t *testing.T) {
	fname := "filename"
	want := []fileIDPair{
		{fname, systemID{value: "foo-user", kind: userType}},
		{fname, systemID{value: "foo-group", kind: groupType}},
	}
	got := extractSysvinitIDsFromFile(fname, []byte(sampleSyvinitFileContents))

	// TODO: use cmp.Diff instead.
	if len(got) != len(want) {
		t.Errorf("Want %d tags, got %d", len(want), len(got))
	}
	if (len(got) > 0 && got[0] != want[0]) ||
		(len(got) > 1 && got[1] != want[1]) {

		t.Errorf("Got: %#v\nWant: %#v\n", got, want)
	}
}

const sampleSystemdUnitFileContents = `
[Unit]
Description=Daemon for generating UUIDs
Documentation=man:uuidd(8)
Requires=uuidd.socket

[Service]
ExecStart=/usr/sbin/uuidd --socket-activation --cont-clock
Restart=no
User=invalid-user
Group=invalid-group
ProtectSystem=strict
ProtectHome=yes
PrivateDevices=yes
PrivateUsers=yes
ProtectKernelTunables=yes
ProtectKernelModules=yes
ProtectControlGroups=yes
MemoryDenyWriteExecute=yes
ReadWritePaths=/var/lib/libuuid/
SystemCallFilter=@default @file-system @basic-io @system-service @signal @io-event @network-io

[Install]
Also=uuidd.socket
`

func TestExtractSystemdIDsFromFile(t *testing.T) {
	fname := "filename"
	want := []fileIDPair{
		{fname, systemID{value: "invalid-user", kind: userType}},
		{fname, systemID{value: "invalid-group", kind: groupType}},
	}
	got := extractSystemdIDsFromFile(fname, []byte(sampleSystemdUnitFileContents))

	opts := cmp.AllowUnexported(fileIDPair{}, systemID{})
	if diff := cmp.Diff(want, got, opts); diff != "" {
		t.Errorf("extractSystemdIDsFromFile.check() mismatch (-want +got):\n%s", diff)
	}
}
