Hi there! Please read on if you'd like to contribute to adequate.

# Where to get the source

Start by grabbing the source code from https://salsa.debian.org/debian/adequate.

The code is structured in a pretty flat way: one file per tag, and one file per
topical, cross-tag functionality (e.g. fs.go for filesystem related
functionality). It's a pretty small codebase so we can afford to have all files
in the main package.

# What to work on

- look for TODOs in the source code (`grep TODO *.go`)
- go through the open bugs, which you can find from
  https://tracker.debian.org/pkg/adequate

Please drop me an email (sez@d.o) before working on anything non-trivial.

# How to test changes

## Manual testing (quick, basic)

You can compare the output of the latest released version of adequate against
the output of a build with local changes, and look for (un)expected diffs. Run
with --all to check all packages. This will only take you so far, because it's
unlikely that all tags will trigger in any given installation, but it's a good
quick test.

```
adequate --all | sort >upstream
go build && ./adequate --all | sort >modified
diff upstream modified
```

## Automated testing (slow, thorough)

adequate has unit tests and autopkgtests. The unit test coverage is pretty poor,
but the autopkgtest coverage is not bad at all. (If you've never heard of
autopkgtests search for DEP8 Debian.)

You'll need an chroot to run the tests locally. There's several ways to create
one; I recommend sbuild. If you don't have one already, look for the relevant
instructions in the Debian wiki. Given a chroot that's called
unstable-amd64-sbuild, you can:

- build the binary package from source in a clean environment (e.g. to make sure
  that the package is not missing any dependency declarations that might happen
  to be installed in your local system) with
  `sbuild -c unstable-amd64-sbuild --chroot-mode=schroot`

- run all of the tests from the adequate source directory with
  `sudo autopkgtest --ignore-restrictions=breaks-testbed,needs-root . 
  	-- schroot unstable-amd64-sbuild`

Occasionally, it's useful to run only a specific autopkgtest test case. In that
case, it's a lot quicker to generate the test binary package, install it
locally, and run adequate against it. E.g. in the local clone of adequate, run:

```
make -C tests changes
sudo dpkg -i tests/adequate-testpkg-missing-alternative_1_all.deb
go build
./adequate --tags=obsolete-conffile adequate-testpkg-missing-alternative
```

## Special cases

I'm not aware of a good way to test the apt hook related changes, short of
manually copying a locally-built adequate binary at /usr/bin/adequate and
running an apt upgrade for real in a way that triggers installation of a package
that in turn triggers an adequate tag (e.g. downgrading a package to make `apt
upgrade` cause an upgrade, which in turn triggers the apt hook).

Testing the debconf-related code requires adding packages to be tested to
/var/lib/adequate/pending:

```
go build *go \
	&& echo $tag_triggering_package | sudo tee /var/lib/adequate/pending \
	&& sudo ./adequate --debconf --pending
```

# Adding a new tag

1. add the tag to `tags.go`
1. create a new file with the relevant functionality (unless it's similar to an
   existing set of tags) and call it from adequate.go
1. add the tag to `doc/tags.desc` and run `make -C adequate.md`, followed by
   `make -C doc adequate.1` (the former requires go-md2man to be installed); you
   can preview the manpage with `make -C doc manpage-preview`
1. update autopkgtest, if possible, and if so, run private/update-coverage to
   update `tests/coverage.txt`
1. add to the package description in `debian/control`

# Portability

We target the go version provided by gccgo in Debian unstable (that is go 1.18
by gccgo-14 as of early 2024). Dependencies on packages outside the standard
library are not to be taken on lightly, because they might introduce an
(indirect) dependency to newer go versions, whether that is new functionality in
the core language (notably generics) or in the standard library.

# Patches

adequate is a Debian native package, so any DD can push changes directly to git,
and that is very much encouraged! If you're not a DD, you can choose any of:

- mail patches to sez@d.o, cc'ing debian-qa@lists.debian.org (you can use
  git-format-patch to generate them from a local clone)
- attach patches to an adequate bug (if a relevant one exists)
- clone adequate in salsa, and send a merge request
