package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestDropAmbiguousPkgs(t *testing.T) {
	pkgs := []string{
		"bar",
		"bar",
		"baz",
		"baz:arm64",
		"foo",
		"foo",
		"foo:amd64",
		"foo:i386",
	}
	want := []string{
		"bar",
		"baz:arm64",
		"foo:amd64",
		"foo:i386",
	}
	got := uniqArchFreePkgs(pkgs)

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("uniqArchFreePkgs.check() mismatch (-want +got):\n%s", diff)
	}
}
