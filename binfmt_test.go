package main

import (
	"strings"
	"testing"
)

func TestBinfmtFormats(t *testing.T) {
	fakeRunner := func([]string) ([]string, error) {
		return strings.Split(`cli (enabled):
     package = mono-runtime
        type = magic
      offset = 0
       magic = MZ
        mask = 
 interpreter = /usr/bin/cli
    detector = /usr/lib/cli/binfmt-detector-cli
jar (enabled):
     package = openjdk-11
        type = magic
      offset = 0
       magic = PK\x03\x04
        mask = 
 interpreter = /usr/bin/jexec
    detector = 
llvm-16-runtime.binfmt (enabled):
     package = llvm-16-runtime
        type = magic
      offset = 0
       magic = BC
        mask = 
 interpreter = /usr/bin/lli-16
    detector = 
llvm-17-runtime.binfmt (enabled):
     package = llvm-17-runtime
        type = magic
      offset = 0
       magic = BC
        mask = 
 interpreter = /usr/bin/lli-17
    detector = 
love (enabled):
     package = love
        type = extension
      offset = 0
       magic = love
        mask = 
 interpreter = /usr/bin/love
    detector = 
python3.12 (enabled):
     package = python3.12
        type = magic
      offset = 0
       magic = \xcb\x0d\x0d\x0a
        mask = 
 interpreter = /usr/bin/python3.12
    detector = 
wine (enabled):
     package = wine
        type = magic
      offset = 0
       magic = MZ
        mask = 
 interpreter = /usr/bin/wine
    detector = 
`, "\n"), nil
	}

	cc := binfmtChecker{
		displayBinfmts: fakeRunner,
	}

	got := cc.binfmtFormats()
	want := []format{
		{name: "cli", interpreter: "/usr/bin/cli", detector: "/usr/lib/cli/binfmt-detector-cli", pkg: "mono-runtime"},
		{name: "jar", interpreter: "/usr/bin/jexec", pkg: "openjdk-11"},
		{name: "llvm-16-runtime.binfmt", interpreter: "/usr/bin/lli-16", pkg: "llvm-16-runtime"},
		{name: "llvm-17-runtime.binfmt", interpreter: "/usr/bin/lli-17", pkg: "llvm-17-runtime"},
		{name: "love", interpreter: "/usr/bin/love", pkg: "love"},
		{name: "python3.12", interpreter: "/usr/bin/python3.12", pkg: "python3.12"},
		{name: "wine", interpreter: "/usr/bin/wine", pkg: "wine"},
	}

	if len(got) != len(want) {
		t.Errorf("Want %d formats, got %d", len(want), len(got))
		t.Errorf("Got %#v\n\n, want %#v\n\n", got, want)
	} else {
		for i := range got {
			if got[i] != want[i] {
				t.Errorf("Got %#v\n\n, want %#v\n\n", got, want)
				break
			}
		}
	}
}
