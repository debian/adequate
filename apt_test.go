package main

import (
	"bufio"
	"strings"
	"testing"
)

func TestAptHookIsEnabled(t *testing.T) {
	tests := []struct {
		name string
		hook string
		want bool
	}{
		{
			name: "enabled",
			hook: "Adequate::Enabled=true\n\n",
			want: true,
		},
		{
			name: "disabled",
			hook: "Adequate::Enabled=false\n\n",
			want: false,
		},
		{
			name: "undefined",
			hook: "blahblah\nblahblahblah\n\n",
			want: false,
		},
	}
	for _, tt := range tests {
		if got := aptHookIsEnabled(bufio.NewReader(strings.NewReader(tt.hook))); got != tt.want {
			t.Errorf("%s: got %v, want %v", tt.name, got, tt.want)
		}
	}
}
